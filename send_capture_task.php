<?php

/**
 * @Copyright: Copyright :copyright: 2019 by IBPort. All rights reserved.
 * @Author: Neal Wong
 * @Email: ibprnd@gmail.com
 */

require_once './vendor/autoload.php';

$host = '<rabbitmq_host>';
$username = '<rabbitmq_username>';
$password = '<rabbitmq_password>';
$virtualHost = '<rabbitmq_virtual_host>';
$exchange = '<rabbitmq_exchange>';
$routingKey = '<rabbitmq_routing_key>';
$task = '<celery_task_name>';
$args = array('Hello World!');
$async = true;

$c = new \Celery\Celery($host, $username, $password, $virtualHost, $exchange, $routingKey);
$c->PostTask($task, $args, $async, $routingKey);
